'use strict';

/**
 * service-sub controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::service-sub.service-sub');
