'use strict';

/**
 * technology-sub router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::technology-sub.technology-sub');
