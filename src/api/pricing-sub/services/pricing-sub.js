'use strict';

/**
 * pricing-sub service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::pricing-sub.pricing-sub');
