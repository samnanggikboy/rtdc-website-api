'use strict';

/**
 * pricing-sub router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::pricing-sub.pricing-sub');
