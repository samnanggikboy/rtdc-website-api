'use strict';

/**
 * pricing-sub controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::pricing-sub.pricing-sub');
