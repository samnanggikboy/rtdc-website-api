'use strict';

/**
 * technology-sub service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::technology-sub.technology-sub');
