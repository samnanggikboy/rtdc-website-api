'use strict';

/**
 * technology-sub controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::technology-sub.technology-sub');
