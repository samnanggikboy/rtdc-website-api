'use strict';

/**
 * service-sub router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::service-sub.service-sub');
